#!/usr/bin/env bash
#Selective initialization of submodules
#Written by by Marko Kosunen, marko.kosunen@aalto.fi, 2017
DIR=$( cd `dirname $0` && pwd )

# THESDK modules are separated here, so that we can use 
# thesdk_helpers/shell/init_dev_branches script, and not
# create dev branches for THESDK modules
THESDKMODULES="\
    ./Entities/rtl \
    ./Entities/spice \
    ./Entities/thesdk \
    ./thesdk_helpers \
"

SUBMODULES="\
    ./Entities/jtag \
    ./Entities/jtag_programmer \
    ./Entities/acorechip \
    ./Entities/acoretestbenches \
    ./Entities/acoretests \
    ./Entities/acoresoftware \
"

git submodule sync
for mod in $SUBMODULES $THESDKMODULES; do 
    git submodule update --init $mod
    cd ${mod}
    if [ -f ./init_submodules.sh ]; then
        ./init_submodules.sh
    fi
    cd ${DIR}

done
exit 0

