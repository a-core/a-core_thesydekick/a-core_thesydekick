#!/usr/bin/env bash

THISREPO="$( basename $(pwd))"
REPOS="\
    Entities/ACoreChip \
    Entities/acorechip \
    Entities/acorechip/chisel \
    Entities/acorechip/chisel/ACoreBase \
    Entities/acorechip/chisel/ProgMem \
    Entities/acorechip/chisel/JTAG \
    Entities/acoresoftware \
    Entities/acoresoftware/lib/a-core-library \
    Entities/acoresoftware/sw/riscv-assembly-tests \
    Entities/acoresoftware/sw/riscv-c-tests \
    Entities/acoresoftware/sw/riscv-coremark-tests \
    Entities/acoresoftware/sw/riscv-tests \
    Entities/ACoreTestbenches \
    Entities/acoretestbenches \
    Entities/ACoreTests \
    Entities/acoretests \
    Entities/chisel_methods \
    Entities/JTAG \
    Entities/jtag \
    Entities/jtag_programmer \
    Entities/rtl \
    Entities/spice \
    Entities/thesdk \
    Entities/thesdk_helpers \
    "

rm -f ./gource_combined_tmp.txt
rm -f ./gource_combined_log.txt

gource --output-custom-log ./gource_combined_tmp.txt --max-files 0 ./
for repo in ${REPOS}; do
    echo $repo
    repologname="$(echo $repo | sed 's#/#_#g')"
    gource --output-custom-log ./${repologname}_log.txt --max-files 0 ./${repo}
    #sed -i -r "s#(.+)\|#\1|/${repo}#" ${repologname}_log.txt
    cat ${repologname}_log.txt >> ./gource_combined_tmp.txt
    rm -f  ${repologname}_log.txt 
done

sed -i -r "s#(.+)\|#\1|/"${THISREPO}"#" ./gource_combined_tmp.txt
sort -n ./gource_combined_tmp.txt > ./gource_combined_log.txt
rm -f ./gource_combined_tmp.txt

#gource -s 0.2 gource_combined_log.txt -hide usernames -start-date 2021-02-17 -stop-date 2022-10-25 -stop-at-end -output-framerate 25 
gource -s 0.2 gource_combined_log.txt -hide usernames -start-date 2021-02-17 -stop-at-end -output-framerate 25

exit 0



