#!/usr/bin/env bash
#############################################################################
# This is the script to install TheSyDeKick dependencies for the user 
# 
# Created by Marko Kosunen, 2017
#############################################################################
##Function to display help with -h argument and to control 
##The configuration from the command line
help_f()
{
cat << EOF
 PIP3USERINSTALL Release 1.0 (16.01.2020)
 TheSyDeKick dependency installer
 Written by Marko Pikkis Kosunen

 SYNOPSIS
   pip3userinstall.sh [OPTIONS]
 DESCRIPTION
   Installs required Python packages over pip.
 OPTIONS
   -u  
       Upgrade also the existing packages. 
       Default: just install the missing ones.
   -h
       Show this help.
EOF
}

THISDIR=$(cd `dirname $0` && pwd)

# Defines TheSDK environment variables
. ${THISDIR}/TheSDK.config

# Set PIPUSEROPT to --user if installing without venv
PIP="${PYI} -m pip install ${PIPUSEROPT}"
UPGRADE=""
while getopts uh opt
do
  case "$opt" in
    u) UPGRADE="--upgrade";;
    h) help_f; exit 0;;
    \?) help_f; exit 0;;
  esac
  shift
done

# Install python dependencies through pip
${PIP} ${UPGRADE} -r requirements.txt

# Installs cocotbify
git clone https://github.com/Martoni/chisverilogutils.git ${HOME}/.local/chisverilogutils
cd ${HOME}/.local/chisverilogutils/cocotbify && ${PIP} -e .

cd ${THESDKHOME}/Entities/jtag && ./pip3userinstall.sh

exit 0

