Welcome to A-Core documentation!
===================================

**A-Core** is a 32-bit RISC-V microprocessor developed at Aalto University, designed to be easy to use, configure, and modify. Its TheSyDeKick environment has been developed to offer low-effort integration to any other TheSyDeKick project.

Read :doc:`setup` section for installation information.

.. note::

   This project, as well as the documentation, is under active development.

.. figure:: pics/acorechip.svg
   :align: center

   Block diagram of A-Core

Contents
--------

.. toctree::
   :maxdepth: 2


   setup
   docker
   getting_started
   chiseltest
   acoretestbenches/intro
   acoresoftware/intro
   acoretests/intro
   acorechip/intro
   jtag/index
   jtag_programmer/index
