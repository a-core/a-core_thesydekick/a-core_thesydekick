Getting started
===============

This is a brief guide on how to run your first program on A-Core.

1. Set up your environment by :doc:`installing tools locally<setup>`, using ECD cluster machine, or using :doc:`the A-Core docker image<docker>`.

2. Run the following:

.. code-block:: bash

    ./init_submodules.sh
    ./configure
    ./pip3userinstall.sh

- ``./init_submodules.sh`` initializes all submodules. It also compiles all Chisel modules, so it takes a while to execute.
- ``./configre`` defines global environmental variables that are used by TheSyDeKick scripts and exports them to ``TheSDK.config``
- ``./pip3userinstall.sh`` installs pip programs that are required by A-Core environment. It also compiles a C-accelerated Python package, `jtag_kernel`.

.. note:: 

    Set ``PIPUSEROPT=""`` in the ``configure`` file if you are using a virtual environment. By default, it is set as "user", so the script installs the pip packages into user install directory with ``pip install --user``. This will throw an error if using a virtual environment.

3. Navigate to ``Entities/acoretests``
4. Run ``make``

This step should run a simulation which runs an assembly program that tests RISC-V M-extension related instructions on A-Core. If no errors are thrown, a GTKWave window, with multiple waveforms visible, should open. Additionally, you should see several ``OK`` prints on the terminal. You can close GTKWave to finish the simulation.

.. figure:: pics/getting_started/gs_gtkwave.png
    :alt: Screenshot of GTKWave
    :align: center

    Screenshot of GTKWave

.. figure:: pics/getting_started/gs_terminal.png
    :width: 500
    :alt: Screenshot of terminal showing OK's
    :align: center

    Screenshot of terminal showing OK's


