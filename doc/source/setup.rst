Setup
=====

This guide explains how to setup your computer to run A-Core projects. There is also a :doc:`Docker image <docker>` available.

Git
---

You need to have git configuration set up. For this project, you need both **an account** and **an ssh key** set up in the following sites:

- `GitLab <https://gitlab.com>`_
- `GitHub <https://github.com>`_

If you work on closed-source projects, you also need them in:

- `bubba.ecdl.hut.fi <https://bubba.ecdl.hut.fi:81/>`_ (Accessable only in Aalto network)
- `version.aalto.fi <https://version.aalto.fi>`_

SSH key setup
-------------
As said, you need to set up SSH keys. For that, you need to generate one on your local machine. You need to do it once per machine, and then use the same key in all sites. 

Read instructions on how to generate an ssh key `here <https://docs.gitlab.com/ee/user/ssh.html>`_. Bubba supports only RSA, so use that.

RISC-V GNU toolchain
--------------------
.. note::

   You need to install this only if you plan on using your local computer for developing A-Core.

The riscv-gnu-toolchain must be built from source. Steps for installing go as follows:

1. Clone the repository: https://github.com/riscv-collab/riscv-gnu-toolchain 
2. Checkout a release version, e.g. ``git checkout 2024.04.12``
3. Open ``README.md`` and ensure you have the prerequisites
4. Run the following command. Set the prefix to a location where you have write access.

.. code-block:: bash

    ./configure --prefix=/opt/riscv --with-multilib-generator="rv32imf_zicsr-ilp32f--c;rv32im_zicsr-ilp32--c;rv32i_zicsr-ilp32--c;rv32if_zicsr-ilp32f--c;"

5. Run ``make``.
6. Wait. It can take over an hour to build.
7. Add the binaries to PATH

Other Tools
-----------

.. note::

   You need to install these only if you plan on using your local computer for developing A-Core.

You need:

- One of the following simulators:

  - Icarus Verilog (v12)
  - Verilator (>=v5.026)
  - Questa Sim

- GTKWave for waveforms
- Java Development Kit (openjdk 17, newer ones do not work)
- sbt
- Python >=3.9
- Python >=3.9 development package (``sudo apt install python3.x-dev``)


