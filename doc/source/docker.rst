A-Core docker
=============

There is a docker image with open-source tools pre-installed. This guide instructs how to use it.

The available docker image is for x86 architecture only. However, you can build it yourself for other architectures.

Repository
----------

The repository is hosted `here <https://gitlab.com/a-core/a-core_chisel/ci-test-container>`_. The repository automatically publishes a new version to GitLab registry every time a new commit is pushed to ``master`` branch. 

Most of the tools (except riscv-gnu-toolchain and sbt) are installed from a `custom build of oss-cad-suite <https://gitlab.com/a-core/oss-cad-suite-build>`_.

Using the pre-built image (x86 only)
------------------------------------

1. Pull the image

.. code-block:: bash

    docker pull registry.gitlab.com/a-core/a-core_chisel/ci-test-container:latest

.. _start_container:

2. Start a container.

You should familiarize yourself with different run options. See e.g. `docker run <https://docs.docker.com/reference/cli/docker/container/run/>`_.

Here is an example command:

.. code-block:: bash

    docker run --rm -it --network host registry.gitlab.com/a-core/a-core_chisel/ci-test-container:latest bash

Explanations of the options:

- ``--rm`` removes the container after finishing the session
- ``-it`` opens an interactive terminal in the container
- ``--network host`` allows connecting to internet inside the container

Note that, with these settings, you do not have access to any local filesystem (however, you can clone from git using https), and you cannot open any graphical programs (such as GTKWave). Both are possible with correct options (see ``-v`` for file mounting). The example command is just to provide a starting point.

3. Set up the environment inside the container

.. code-block:: bash

    source /oss-cad-suite/environment
    export CPLUS_INCLUDE_PATH=/oss-cad-suite/include/python3.11
    git config --global url."https://github.com/".insteadOf "git@github.com:"
    git config --global url."https://gitlab.com/".insteadOf "git@gitlab.com:"

Building the image
------------------

1. Clone the repository

.. code-block:: bash

    git clone git@gitlab.com:a-core/a-core_chisel/ci-test-container.git

2. Run

.. code-block:: bash

    docker build -t ci-test-container:latest .

This will take a while.

3. Follow the instructions from :ref:`using pre-built image guide step 2 <start_container>`

The image name will be ``ci-test-container`` instead of ``registry.gitlab.com/a-core/a-core_chisel/ci-test-container``.




