# Set paths for Mentor programs (Eldo)
use advms_17.1
# Set paths for Cadence programs (Spectre)
use icadv123
# Get updated iverilog version
use icarus

#Set module thesdk to PYTHONPATH
set called=($_)
set scriptfp=`readlink -f $called[2]`
set scriptdir=`dirname $scriptfp`
set gnu_toolchain_path="/prog/riscv/bin"
set verilator_path="/prog/verilator/bin"
set gcc_path="/opt/rh/devtoolset-7/root/usr/bin"
set pip_path="${HOME}/.local/bin"
set sby_path="/prog/riscv/SymbiYosys/bin"
set yosys_path="/prog/riscv/yosys/bin"
set boolector_path="/prog/riscv/boolector/bin"

if ( ! $?PYTHONPATH ) then
    setenv PYTHONPATH $scriptdir/Entities/thesdk
else
    setenv PYTHONPATH $scriptdir/Entities/thesdk:${PYTHONPATH}
endif

if ( -d ${HOME}/.local/bin && "${PATH}" !~ *"${HOME}/.local/bin"* ) then
    echo "Adding \${HOME}/.local/bin to path for user specific python installations"
    setenv PATH ${HOME}/.local/bin:${PATH}
else
    echo "${HOME}/.local/bin already in path"
endif

# Add riscv-gnu-toolchain installation directory to path
if ( -d ${gnu_toolchain_path} && "${PATH}" !~ *${gnu_toolchain_path}* ) then
    echo "Adding ${gnu_toolchain_path} to path"
    setenv PATH ${gnu_toolchain_path}:${PATH}
else
    echo "${gnu_toolchain_path} already in path"
endif

# Add verilator to path
if ( -d ${verilator_path} && "${PATH}" !~ *${verilator_path}* ) then
    echo "Adding ${verilator_path} to path"
    setenv PATH ${verilator_path}:${PATH}
else
    echo "${verilator_path} already in path"
endif

# Add newer version of gcc to path (required by verilator)
if ( -d ${gcc_path} && "${PATH}" !~ *${gcc_path}* ) then
    echo "Adding ${gcc_path} to path"
    setenv PATH ${gcc_path}:${PATH}
else
    echo "${gcc_path} already in path"
endif

# Add binaries of pip programs to path
if ( -d ${pip_path} && "${PATH}" !~ *${pip_path}* ) then
    echo "Adding ${pip_path} to path"
    setenv PATH ${pip_path}:${PATH}
else
    echo "${pip_path} already in path"
endif

if ( -d ${sby_path} && "${PATH}" !~ *${sby_path}* ) then
    echo "Adding ${sby_path} to path"
    setenv PATH ${sby_path}:${PATH}
else
    echo "${sby_path} already in path"
endif

if ( -d ${yosys_path} && "${PATH}" !~ *${yosys_path}* ) then
    echo "Adding ${yosys_path} to path"
    setenv PATH ${yosys_path}:${PATH}
else
    echo "${yosys_path} already in path"
endif

if ( -d ${boolector_path} && "${PATH}" !~ *${boolector_path}* ) then
    echo "Adding ${boolector_path} to path"
    setenv PATH ${boolector_path}:${PATH}
else
    echo "${boolector_path} already in path"
endif

unset called
unset scriptfp
unset scriptdir
unset gnu_toolchain_path
unset verilator_path
unset gcc_path
unset pip_path
unset sby_path
unset yosys_path
unset boolector_path