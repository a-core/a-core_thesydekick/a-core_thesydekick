#!/usr/bin/bash

source /oss-cad-suite/environment
export CPLUS_INCLUDE_PATH=/oss-cad-suite/include/python3.11
git config --global url."https://github.com/".insteadOf "git@github.com:"
git config --global url."https://gitlab.com/".insteadOf "git@gitlab.com:"
./init_submodules.sh
# Set PIPUSEROPT empty
sed -i '/PIPUSEROPT/s/"[^"]*"/""/' configure
./configure
./pip3userinstall.sh
cd Entities/acoresoftware/test_configs/riscv-tests && python3 gen_test_configs.py && cd -
cd Entities/acoretestbenches/sim_configs && sed -i "s/interactive_sim: true/interactive_sim: false/" thesdk.yml && cd -
cd Entities/acoretests && ./configure && python3 acoretests/generic_tests.py --test_configs "../acoresoftware/test_configs/riscv-tests/*.yml"
