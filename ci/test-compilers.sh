#!/usr/bin/bash

source /oss-cad-suite/environment
export CPLUS_INCLUDE_PATH=/oss-cad-suite/include/python3.11
git config --global url."https://github.com/".insteadOf "git@github.com:"
git config --global url."https://gitlab.com/".insteadOf "git@gitlab.com:"
./init_submodules.sh
# Set PIPUSEROPT empty
sed -i '/PIPUSEROPT/s/"[^"]*"/""/' configure
./configure
./pip3userinstall.sh
cd Entities/acorechip/acorechip && python3 acore_compilers.py