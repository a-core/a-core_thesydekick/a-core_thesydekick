#!/usr/bin/bash
git config --global url."https://github.com/".insteadOf "git@github.com:"
git config --global url."https://gitlab.com/".insteadOf "git@gitlab.com:"
./init_submodules.sh
./configure
# cd doc && sphinx-build -b html source ../public