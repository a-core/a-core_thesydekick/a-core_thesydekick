# A-Core RISC-V processor - TheSydeKick-System Development Kit Project

## [Documentation](https://a-core-risc-v.readthedocs.io/stable/)

## Brief introduction

Initiated by Marko Kosunen, marko.kosunen@aalto.fi 7.8.2017

* OBS: THE SCRIPTS TO BE SOURCED ARE WRITTEN FOR T-SHELL *
if you're using some other shell, change to tcsh or modify the scripts to be 
compliant with your shell.
```
tcsh
```

This is a very brief readme. For background information, see the wikipage
[https://github.com/TheSystemDevelopmentKit/thesdk_template/wiki/The-System-Development-Kit](https://github.com/TheSystemDevelopmentKit/thesdk_template/wiki/The-System-Development-Kit "Wiki")


## Configuration quickstart

The project has been developed in a way to support LSF, but it doesn't need to be used

In TheSDK.config (generated with `./configure`), you can modify different constants, such as 
- `LSFSUBMISSION` - command to submit a job to LSF. Empty by default.
- `LSFINTERACTIVE` - command to submit a job to LSF, used with _interactive_ runs. Empty by default.
- `PYL` - Python command
- `PY` - Python command with LSF submission
- `PYI` - Python command with interactive LSF submission
- `PIPUSEROPT` - Parameter to be provided to pip install scripts. By default it is `--user` to install packages to user directories. **If you use a virtual Python environment, you should set this empty**

Run:
```
./init_submodules.sh
./configure
./pip3userinstall.sh
source sourceme.csh (if using ECD machines)
```

To test whether your setup works, run the following:
```
cd Entities/acoretests
./configure && make
```

Upon completion, it should open GTKWave automatically.
